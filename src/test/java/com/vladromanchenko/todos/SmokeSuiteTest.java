package com.vladromanchenko.todos;

import com.vladromanchenko.todos.categories.Buggy;
import com.vladromanchenko.todos.categories.Smoke;
import com.vladromanchenko.todos.features.TodosE2ETest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by v.romanchenko on 4/30/2016.
 */

@RunWith(Categories.class)
@Suite.SuiteClasses(TodosE2ETest.class)
@Categories.IncludeCategory(Smoke.class)
@Categories.ExcludeCategory(Buggy.class)
public class SmokeSuiteTest {
}
