package com.vladromanchenko.todos;

import com.vladromanchenko.todos.features.TodosE2ETest;
import com.vladromanchenko.todos.features.TodosOpeartionsAtAllFilterTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by v.romanchenko on 4/30/2016.
 */
@RunWith(Categories.class)
@Suite.SuiteClasses({TodosE2ETest.class, TodosOpeartionsAtAllFilterTest.class})
public class AllSuiteTest {
}
