package com.vladromanchenko.todos.pages;


import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static com.vladromanchenko.todos.pages.Todos.filterActive;
import static com.vladromanchenko.todos.pages.Todos.filterCompleted;


/**
 * Created by v.romanchenko on 4/26/2016.
 */
public class TodosGivens {

    public static void givenAtAll(Task... tasks) {
        ensurePageOpened();
         ArrayList<String> list = new ArrayList<String>();

        for (Task task : tasks) {
            list.add(task.toString());
        }

        String createTaskCommand = "localStorage.setItem(\"todos-troopjs\", \"[" + String.join(",", list) + "]\")";
        executeJavaScript(createTaskCommand);
        refresh();
    }

    public static void givenAtActive(Task... tasks) {
        givenAtAll(tasks);
        filterActive();
    }

    public static void givenAtCompleted(Task... tasks) {
        givenAtAll(tasks);
        filterCompleted();
    }

    public static void givenAtAll(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
    }

    public static void givenAtActive(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
        filterActive();
    }

    public static void givenAtCompleted(Status taskStatus, String... taskTexts) {
        givenAtAll(aTasks(taskStatus, taskTexts));
        filterCompleted();
    }

    public static Task aTask(Status taskStatus, String text) {
        return new Task(taskStatus, text);
    }

    public static Task[] aTasks(Status taskStatus, String... text) {
        Task[] tasks = new Task[text.length];
        for (int i = 0; i < text.length; i++) {
            tasks[i] = aTask(taskStatus, text[i]);
        }
        return tasks;
    }

    public static void ensurePageOpened() {
        if (!url().equals("https://todomvc4tasj.herokuapp.com/")) {
            open("https://todomvc4tasj.herokuapp.com/");
        }

    }

    public enum Status {
        ACTIVE("false"), COMPLETED("true");

        private String flag;

        Status(String flag) {
            this.flag = flag;
        }

        @Override
        public String toString() {
            return flag;
        }
    }

    public static class Task {

        private String actualString = "";

        public Task(Status state, String text) {
            this.actualString = "{\\\"completed\\\":" + state + ", \\\"title\\\":\\\"" + text + "\\\"}";
        }

        @Override
        public String toString() {
            return actualString;
        }
    }

}
