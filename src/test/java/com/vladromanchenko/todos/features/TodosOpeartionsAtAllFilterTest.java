package com.vladromanchenko.todos.features;

import com.vladromanchenko.todos.categories.Buggy;
import com.vladromanchenko.todos.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.vladromanchenko.todos.pages.Todos.*;
import static com.vladromanchenko.todos.pages.TodosGivens.Status.ACTIVE;
import static com.vladromanchenko.todos.pages.TodosGivens.Status.COMPLETED;
import static com.vladromanchenko.todos.pages.TodosGivens.aTask;
import static com.vladromanchenko.todos.pages.TodosGivens.givenAtAll;

/**
 * Created by v.romanchenko on 4/30/2016.
 */

public class TodosOpeartionsAtAllFilterTest extends BaseTest {

    @Test
    public void testCompleteAllAtAllFilter() {
        givenAtAll(ACTIVE, "a", "b", "c");

        toggleAll();
        assertTasks("a", "b", "c");
        assertItemsLeft(0);
    }

    @Test
    public void testClearCompletedAtAllFilter() {
        givenAtAll(COMPLETED, "a", "b", "c");

        clearCompleted();
        assertNoTasks();
    }

    @Test
    public void testReopenAtAllFilter() {
        givenAtAll(COMPLETED, "a", "b", "c");

        toggle("a");
        filterActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    @Category(Buggy.class)
    public void testReopenAllAtAllFilter() {
        givenAtAll(COMPLETED, "a","b");

        toggleAll();
        assertTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED,"b"));

        cancelEdit("a", "a edited");
        assertTasks("a","b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditClickOutsideAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        startEdit("a", "a edited");
        newtodo.click();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testEditPressTabAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED,"b"));

        startEdit("a", "a edited").pressTab();
        assertTasks("a edited", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptyingTaskTextAtAllFilter() {
        givenAtAll(aTask(ACTIVE, "a"), aTask(COMPLETED, "b"));

        edit("a", "");
        assertTasks("b");
    }

    @Test
    public void testSwitchFromAllToCompletedFilter() {
        givenAtAll(aTask(COMPLETED, "a"), aTask(ACTIVE,"b"));

        filterCompleted();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }
}