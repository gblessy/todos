package com.vladromanchenko.todos.features;

import com.vladromanchenko.todos.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.vladromanchenko.todos.pages.Todos.*;
import static com.vladromanchenko.todos.pages.Todos.assertNoTasks;
import static com.vladromanchenko.todos.pages.Todos.delete;
import static com.vladromanchenko.todos.pages.TodosGivens.Status.ACTIVE;
import static com.vladromanchenko.todos.pages.TodosGivens.givenAtAll;

/**
 * Created by v.romanchenko on 4/30/2016.
 */
@Category(Smoke.class)
public class TodosE2ETest extends BaseTest {
    @Test
    public void testLifeCycle() {
        givenAtAll(ACTIVE, "a");
        edit("a", "edited a");
        toggle("edited a");
        assertTasks("edited a");

        filterActive();
        assertNoVisibleTasks();

        add("b");
        assertVisibleTasks("b");

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("edited a", "b");
        //reopen
        toggle("b");
        assertVisibleTasks("edited a");

        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertVisibleTasks("b");

        delete("b");
        assertNoTasks();
    }
}
